<?php
$arrNilai=array("andi"=>80,"Budi"=>90, "Beni"=>90, "Dodi"=>75, "Endi"=>85);
echo "<b>Array sebelum diurutkan</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

sort($arrNilai);
reset($arrNilai);
echo "<b>Array setelah diurutkan dengan sort()</b>";
echo "<pre>";
print_r($arrNilai);
echo "</pre>";

rsort($arrNilai);
reset($arrNilai);
echo "<b>array setelah diurutkan dengan rsort()</b>";
echo "</pre>";
print_r($arrNilai);
echo "</pre>";
?>
